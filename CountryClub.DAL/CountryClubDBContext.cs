﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CountryClub.DAL.Entities;

namespace CountryClub.DAL
{
    public class CountryClubDBContext : IdentityDbContext<Employer>
    {

        public CountryClubDBContext() : base()
        {
        }

        public CountryClubDBContext(DbContextOptions<CountryClubDBContext> options) : base(options)
        {
        }

        public DbSet<Employer> Employers { get; set; }
        public DbSet<CountryClubPlace> CountryClubs { get; set; }
     

        protected override void OnModelCreating(ModelBuilder builder)
        {
      
            builder.Entity<Employer>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            builder.Entity<Employer>()
                .HasOne(e => e.PlaceOfWork)
                .WithMany()
                .HasForeignKey(e => e.PlaceOfWorkId)
                .OnDelete(DeleteBehavior.NoAction);

            base.OnModelCreating(builder);
        }

    }
}
