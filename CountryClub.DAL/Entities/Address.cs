﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CountryClub.DAL.Entities
{
    public class Address
    {
        public int Id { get; set; }
        public int ZipCode { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
