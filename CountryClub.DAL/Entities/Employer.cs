﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CountryClub.DAL.Entities
{
    public class Employer : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PlaceOfWorkId { get; set; }
        [ForeignKey(nameof(PlaceOfWorkId))]
        public CountryClubPlace PlaceOfWork { get; set; }
        public DateTime StartOfJob { get; set; }
        public int AddressId { get; set; }
        [ForeignKey(nameof(AddressId))]
        public Address Address { get; set; }

    }
}
