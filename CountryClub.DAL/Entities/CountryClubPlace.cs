﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CountryClub.DAL.Entities
{
    public class CountryClubPlace
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AddressId { get; set; }
        [ForeignKey(nameof(AddressId))]
        public Address Address { get; set; }
        public string About { get; set; }
        public int YearOfFoundation { get; set; }
    }
}
