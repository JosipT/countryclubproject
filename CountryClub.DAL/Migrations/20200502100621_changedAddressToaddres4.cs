﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountryClub.DAL.Migrations
{
    public partial class changedAddressToaddres4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AddressId",
                table: "CountryClub",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CountryClub_AddressId",
                table: "CountryClub",
                column: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_CountryClub_Address_AddressId",
                table: "CountryClub",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CountryClub_Address_AddressId",
                table: "CountryClub");

            migrationBuilder.DropIndex(
                name: "IX_CountryClub_AddressId",
                table: "CountryClub");

            migrationBuilder.DropColumn(
                name: "AddressId",
                table: "CountryClub");
        }
    }
}
