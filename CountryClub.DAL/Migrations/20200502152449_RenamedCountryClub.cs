﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CountryClub.DAL.Migrations
{
    public partial class RenamedCountryClub : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_CountryClub_PlaceOfWorkId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "CountryClub");

            migrationBuilder.CreateTable(
                name: "CountryClubs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    AddressId = table.Column<int>(nullable: false),
                    About = table.Column<string>(nullable: true),
                    YearOfFoundation = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryClubs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CountryClubs_Address_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Address",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CountryClubs_AddressId",
                table: "CountryClubs",
                column: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_CountryClubs_PlaceOfWorkId",
                table: "AspNetUsers",
                column: "PlaceOfWorkId",
                principalTable: "CountryClubs",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_CountryClubs_PlaceOfWorkId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "CountryClubs");

            migrationBuilder.CreateTable(
                name: "CountryClub",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    About = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AddressId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    YearOfFoundation = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryClub", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CountryClub_Address_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Address",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CountryClub_AddressId",
                table: "CountryClub",
                column: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_CountryClub_PlaceOfWorkId",
                table: "AspNetUsers",
                column: "PlaceOfWorkId",
                principalTable: "CountryClub",
                principalColumn: "Id");
        }
    }
}
