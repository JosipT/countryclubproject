﻿using AutoMapper;
using CountryClub.Base.Models;
using CountryClub.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryClub.Web.AutoMapper
{
    public class EmployerProfile : Profile
    {
        public EmployerProfile()
        {
            CreateMap<EmployerModel, Employer>().ReverseMap();
            CreateMap<EmployerModel, Employer>()
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.PlaceOfWork, opt => opt.MapFrom(src => src.PlaceOfWork));
            CreateMap<CountryClubPlace, CountryClubModel>().
                ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address)).ReverseMap();
            CreateMap<CountryClubPlace, CountryClubModel>().
                ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id)).
                ForMember(dest => dest.Label, opt => opt.MapFrom(src => src.Name));
            CreateMap<AddressModel, Address>().ReverseMap();
            CreateMap<Employer, EmployerModel>().ForMember(dest => dest.StartOfJob,
                opt => opt.MapFrom(src => src.StartOfJob.ToShortDateString()));
        }
    }
}
