﻿using AutoMapper;
using CountryClub.Base.Models;
using CountryClub.Business.Services;
using CountryClub.DAL;
using CountryClub.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryClub.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmployerController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IEmployerService _employerService;
        private readonly CountryClubDBContext _context;
        public EmployerController(IEmployerService employerService, IMapper mapper, CountryClubDBContext context)
        {
            _mapper = mapper;
            _employerService = employerService;
            _context = context;
        }

        /// <summary>
        /// Get all employers
        /// </summary>
        /// returns collection of EmployerModel objects
        [HttpGet]
        public IActionResult GetAllEmployers()
        {
            var result = _employerService.GetAllEmployers();
            List<EmployerModel> employers = new List<EmployerModel>();
            _mapper.Map(result, employers);
            return Ok(employers);
        }


        /// <summary>
        /// Get employer by Id
        /// </summary>
        /// <param name="id"></param>
        /// returns EmployerModel
        [HttpGet("id")]
        public IActionResult GetEmployerById(string id)
        {
            var employer = _context.Employers.Include(e => e.Address).Where(e => e.Id == id).FirstOrDefault();
            EmployerModel employerModel = _mapper.Map<EmployerModel>(employer);
            return Ok(employerModel);
        }

        /// <summary>
        /// Create employer
        /// </summary>
        /// <param name="employerModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EmployerModel employerModel)
        {
            Employer employer = new Employer();
            _mapper.Map(employerModel, employer);
            var result = await _employerService.CreateEmployer(employer);
            if (!result.Succeeded)
            {
                return BadRequest(result);
            }
            Employer reloaded_employer = _context.Employers.Include(e => e.Address).Include(e => e.PlaceOfWork).ThenInclude(p => p.Address).FirstOrDefault(e => e.Id == employer.Id);
            return CreatedAtAction("Create employer", _mapper.Map<EmployerModel>(reloaded_employer));
        }

               
        /// <summary>
        /// Update employer based on id
        /// </summary>
        /// <param name="employerModel"></param>
        /// <param name="id"></param>
        /// returns updated employer       
        [HttpPut("employer/{id}")]
        public async Task<IActionResult> UpdateEmployer([FromBody] EmployerModel employerModel, string id)
        {
            Employer employer = _context.Employers.Include(e => e.Address).Where(e => e.Id == id).FirstOrDefault();
            if(employer is null)
            {
                return NotFound("Can't update employer");
            }
            _mapper.Map(employerModel, employer);
            var result = await _employerService.UpdateEmployer(id, employer);
            if (!result.Succeeded)
            {
                return BadRequest(result);
            }
            return CreatedAtAction("Updated employer", _mapper.Map<EmployerModel>(employer));

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Employer>> DeleteEmployer(string id)
        {
            Employer employer = await _context.Employers.FindAsync(id);
            if (employer == null)
            {
                return NotFound("Can't delete employer");
            }

            try
            {
                _context.Employers.Remove(employer);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return BadRequest("Can't delete employer");
            }
            return Ok();
        }
        

    }
}
