﻿using AutoMapper;
using CountryClub.Base.Models;
using CountryClub.Business.Services;
using CountryClub.DAL;
using CountryClub.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryClub.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountryClubController: ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICountryClubService _countryClubService;
        private readonly CountryClubDBContext _context;
        public CountryClubController(ICountryClubService countryClubService, IMapper mapper, CountryClubDBContext context)
        {
            _mapper = mapper;
            _countryClubService = countryClubService;
            _context = context;
        }

        /// <summary>
        /// Get all country clubs
        /// </summary>
        /// returns collection of CountryClubModel objects
        [HttpGet]
        public IActionResult GetAllCountryClubs()
        {
            var result = _countryClubService.GetAllCountryClubs();
            List<CountryClubModel> countryClubs = new List<CountryClubModel>();
            _mapper.Map(result, countryClubs);
            return Ok(countryClubs);
        }


        /// <summary>
        /// Create employer
        /// </summary>
        /// <param name="employerModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CountryClubModel countryClubModel)
        {
            CountryClubPlace countryClub = new CountryClubPlace();
            _mapper.Map(countryClubModel, countryClub);
            var result = await _countryClubService.CreateCountryClub(countryClub);
            if (!result.Succeeded)
            {
                return BadRequest(result);
            }
            return CreatedAtAction("Create country club", _mapper.Map<CountryClubModel>(countryClub));
        }

    }
}
