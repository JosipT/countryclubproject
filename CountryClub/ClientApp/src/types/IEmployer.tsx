import { IAddress } from "./IAddress";
import { ICountryClub } from "./ICountryClub";

export interface IEmployer {
    id: string,
    firstName: string,
    lastName: string,
    placeOfWork: ICountryClub,
    startOfJob: Date,
    address: IAddress
}