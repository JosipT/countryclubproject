export interface IAddress {
    zipCode: number,
    street: string,
    city: string,
    country: string
}