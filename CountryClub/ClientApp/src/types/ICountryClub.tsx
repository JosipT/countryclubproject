import { IAddress } from "./IAddress";

export interface ICountryClub {
    id: number,
    value: number,
    label: string,
    name: string,
    address: IAddress,
    about: string,
    yearOfFoundation: number
}

export interface ICountryClubSelect {
    id: number,
    value: number,
    label: string,
    name: string,
}