import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import EmployerContainer from './containers/employerContainer';

import './custom.css'

export default () => (
    <Layout>
        <Route exact path='/' component={EmployerContainer} />
    </Layout>
);
