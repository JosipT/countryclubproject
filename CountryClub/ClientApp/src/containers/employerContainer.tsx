import React, { useEffect, useState } from 'react';
import EmployerComponent from "../components/EmployerComponent";
import employerApi from "../api/employerApi/index";
import { IEmployer } from '../types/IEmployer';
import { ICountryClub } from '../types/ICountryClub';
import countryClubApi from '../api/countryClubApi';

const EmployerContainer = () => {

    const [data, setData] = useState<IEmployer[]>([]);
    const [update, setUpdate] = useState(false);
    const [clubs, setClubs] = useState<ICountryClub[]>([]);

    const updateEmployer = async (employer: IEmployer) => {
        var response: IEmployer = await employerApi.updateEmployer(employer);
        var index = data.findIndex(e => e.id === response.id);
        if (index !== -1) {
            var data2 = data.slice();
            data2.splice(index, 1, response);
            setData(data2);
        }
        setUpdate(!update);
    }

    useEffect(() => {
        async function getData() {
            var response: IEmployer[] = await employerApi.getAllEmployers();
            var response2: ICountryClub[] = await countryClubApi.getAllClubs();
            setData(response);
            setClubs(response2);
            setUpdate(!update);
        };
        getData();
    }, [])

    return (
        clubs.length !== 0 && <EmployerComponent clubs={clubs} update={update} updateEmployer={updateEmployer} data={data} />
    );

}

export default EmployerContainer;