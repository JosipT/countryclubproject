import React, { useEffect, Fragment } from "react";
import MaterialTable from 'material-table';
import { forwardRef } from 'react';
import {
    AddBox, ArrowDownward, Check, ChevronLeft, ChevronRight,
    Clear, DeleteOutline, Edit, FilterList, FirstPage, LastPage,
    Remove, SaveAlt, Search, ViewColumn
} from '@material-ui/icons';
import CloseIcon from '@material-ui/icons/Close';
import { IEmployer } from "../types/IEmployer";
import { Grid, Divider } from "@material-ui/core";
import "./EmployerComponent.css";
import { ICountryClub, ICountryClubSelect } from "../types/ICountryClub";
import Select from "react-select";

interface IProps {
    data: IEmployer[],
    updateEmployer: (employer: IEmployer) => void,
    update: boolean,
    clubs: ICountryClub[]
}

const EmployerComponent: React.FC<IProps> = (props: IProps) => {

    const [options, setOptions] = React.useState<ICountryClub[]>(props.clubs);

    const [state, setState] = React.useState({
        columns: [
            { title: 'Name', field: 'firstName' },
            { title: 'Surname', field: 'lastName' },
            { title: 'Start Date', field: 'startOfJob', types: Date },
            { title: 'Street', field: 'address.street' },
            { title: 'City', field: 'address.city' },
            { title: 'ZipCode', field: 'address.zipCode' },
            { title: 'Country', field: 'address.country' },
            {
                title: 'Place of work', field: 'placeOfWork.name', editComponent: (props2: any) => (
                    <Fragment>
                        <Select
                            className="basic-single"
                            defaultValue={props2.rowData.placeOfWork}
                            name="color"
                            options={options}
                            onChange={(e) => changePlaceOfWorkData(props2.rowData.id, e, e.id)}
                        />
                    </Fragment>
                )
            },
        ],
        data: props.data
    });

    const changePlaceOfWorkData = (id: string, placeOfWork: ICountryClub, placeOfWorkId: number) => {
        var stateData = state.data.slice();
        var employer: IEmployer = stateData.filter(e => e.id == id)[0];
        setState({
            ...state, data: { ...state.data, ...employer, ...{ placeOfWorkId: placeOfWorkId } }
        });
        console.log(employer);
        console.log("Employer " + id);
        console.log(placeOfWork);
        console.log("ID " + placeOfWorkId);
    }

    const [showData, setShowData] = React.useState(false);
    const [addData, setAddData] = React.useState<IEmployer>();

    useEffect(() => {
        setState((prevState) => {
            return { ...prevState, data: props.data };
        });
        setOptions(props.clubs);
    }, [props.update])

    const tableRef: React.RefObject<any> = React.createRef();

    const tableIcons = {
        Add: forwardRef((props: any, ref: any) => <AddBox {...props} ref={ref} />),
        Check: forwardRef((props: any, ref: any) => <Check {...props} ref={ref} />),
        Clear: forwardRef((props: any, ref: any) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props: any, ref: any) => <DeleteOutline {...props} ref={ref} />),
        DetailPanel: forwardRef((props: any, ref: any) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props: any, ref: any) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props: any, ref: any) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props: any, ref: any) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props: any, ref: any) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props: any, ref: any) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props: any, ref: any) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props: any, ref: any) => <ChevronLeft {...props} ref={ref} />),
        ResetSearch: forwardRef((props: any, ref: any) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props: any, ref: any) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props: any, ref: any) => <ArrowDownward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props: any, ref: any) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props: any, ref: any) => <ViewColumn {...props} ref={ref} />)
    };

    return (
        <>
            <MaterialTable
                icons={tableIcons}
                tableRef={tableRef}
                title="Editable Example"
                columns={state.columns}
                onRowClick={(e, data, tooglePanel) => { setAddData(data); setShowData(true); }}
                data={state.data}
                editable={{
                    onRowAdd: (newData) =>
                        new Promise((resolve) => {
                            setTimeout(() => {
                                resolve();
                                setState((prevState) => {
                                    const data = [...prevState.data];
                                    data.push(newData);
                                    return { ...prevState, data };
                                });
                            }, 600);
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise((resolve) => {
                            setTimeout(() => {
                                resolve();
                                if (oldData) {
                                    props.updateEmployer(newData);
                                }
                            }, 600);
                        }),
                    onRowDelete: (oldData) =>
                        new Promise((resolve) => {
                            setTimeout(() => {
                                resolve();
                                setState((prevState) => {
                                    const data = [...prevState.data];
                                    data.splice(data.indexOf(oldData), 1);
                                    return { ...prevState, data };
                                });
                            }, 600);
                        }),
                }}
            />
            {showData &&
                <Grid container className="addDataHolder">
                    <Grid item xs={12} container justify="flex-end">
                        <CloseIcon style={{ cursor: "pointer" }} onClick={() => setShowData(false)} />
                    </Grid>
                    <Grid item xs={12} container className="block">
                        <Grid item xs={12}>
                            <div className="title">Podaci o zaposleniku</div>
                        </Grid>
                        <Grid item xs={4} container>
                            <div style={{ display: "inline" }}>Ime: &nbsp;</div>
                            <div style={{ display: "inline" }}>{addData!.firstName}</div>
                        </Grid>
                        <Grid item xs={4}>
                            <div style={{ display: "inline" }}>Prezime: &nbsp;</div>
                            <div style={{ display: "inline" }}>{addData!.lastName}</div>
                        </Grid>
                        <Grid item xs={4}>
                            <div style={{ display: "inline" }}>Početak rada: &nbsp; </div>
                            <div style={{ display: "inline" }}>{addData!.startOfJob}</div>
                        </Grid>
                    </Grid>
                    <Divider className="divider" />
                    <Grid item xs={12} container className="block">
                        <Grid item xs={12}>
                            <div className="title">Adresa zaposlenika</div>
                        </Grid>
                        <Grid item xs={4} container>
                            <div style={{ display: "inline" }}>Ulica:  &nbsp;</div>
                            <div style={{ display: "inline" }}>{addData!.address.street}</div>
                        </Grid>
                        <Grid item xs={4} container>
                            <div style={{ display: "inline" }}>Grad: &nbsp;</div>
                            <div style={{ display: "inline" }}>{addData!.address.city}</div>
                        </Grid>
                        <Grid item xs={4} container>
                            <div style={{ display: "inline" }}>Država: &nbsp;</div>
                            <div style={{ display: "inline" }}>{addData!.address.country}</div>
                        </Grid>
                    </Grid>
                    <Divider className="divider" />
                    <Grid item xs={12} container className="block">
                        <Grid item xs={12}>
                            <div className="title">Mjesto rada zaposlenika</div>
                        </Grid>
                        <Grid item xs={4} container >
                            <div style={{ display: "inline" }}>Naziv: &nbsp;</div>
                            <div style={{ display: "inline" }}>{addData!.placeOfWork.name}</div>
                        </Grid>
                        <Grid item xs={4} container>
                            <div style={{ display: "inline" }}>Opis: &nbsp;</div>
                            <div style={{ display: "inline" }}>{addData!.placeOfWork.about}</div>
                        </Grid>
                        <Grid item xs={4} container>
                            <div style={{ display: "inline" }}>Godina osnutka: &nbsp;</div>
                            <div style={{ display: "inline" }}>{addData!.placeOfWork.yearOfFoundation}</div>
                        </Grid>
                    </Grid>
                </Grid>
            }
        </>
    );
}

export default EmployerComponent;