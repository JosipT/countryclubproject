import { api } from '../api';
import { IEmployer } from '../../types/IEmployer';


export default
    {
        getAllEmployers(): Promise<any> {
            return api.get('Employer/')
        },
        updateEmployer(employer: IEmployer): Promise<any> {
            return api.put('Employer/employer/' + employer.id, employer);
        }
    };

