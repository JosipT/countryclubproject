﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountryClub.Base.Models
{
    public class EmployerModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public CountryClubModel PlaceOfWork { get; set; }
        public int PlaceOfWorkId { get; set; }
        public string StartOfJob { get; set; }
        public AddressModel Address { get; set; }
    }
}
