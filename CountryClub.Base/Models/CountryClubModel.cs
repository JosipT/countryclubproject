﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CountryClub.Base.Models
{
    public class CountryClubModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public string Label { get; set; }
        public AddressModel Address { get; set; }
        public string About { get; set; }
        public int YearOfFoundation { get; set; }
    }
}
