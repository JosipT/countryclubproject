﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using CountryClub.DAL;
using CountryClub.DAL.Entities;

namespace CountryClub.Business.Services
{
    public class EmployerService : IEmployerService
    {

        private readonly CountryClubDBContext _context;

        public EmployerService(CountryClubDBContext context)
        {
            _context = context;
        }

        public IEnumerable<Employer> GetAllEmployers()
        {
            var result = _context.Employers.Include(e => e.Address).Include(e => e.PlaceOfWork).ThenInclude(p => p.Address).ToList();
            return result;
        }

        public async Task<IdentityResult> CreateEmployer(Employer employer)
        {
            try
            {
                Address address = employer.Address;
                 _context.Add(address);
                await _context.SaveChangesAsync();
                int addressId = address.Id;
                employer.AddressId = addressId;
                _context.Add(employer);
                await _context.SaveChangesAsync();
                return IdentityResult.Success;
            } 
            catch (Exception e)
            {
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = e.GetType().ToString(),
                    Description = e.Message
                });
            }
        }

        public async Task<IdentityResult> UpdateEmployer(string id, Employer employer)
        {
            try
            {
                await _context.SaveChangesAsync();
                return IdentityResult.Success;
            }
            catch (Exception e)
            {
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = e.GetType().ToString(),
                    Description = e.Message
                });
            }
        }
    }
}
