﻿using CountryClub.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CountryClub.Business.Services
{
    public interface ICountryClubService
    {
        IEnumerable<CountryClubPlace> GetAllCountryClubs();
        Task<IdentityResult> CreateCountryClub(CountryClubPlace countryClub);
    }
}
