﻿using CountryClub.DAL;
using CountryClub.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CountryClub.Business.Services
{
    public class CountryClubService : ICountryClubService
    {
        private readonly CountryClubDBContext _context;

        public CountryClubService(CountryClubDBContext context)
        {
            _context = context;
        }

        public async Task<IdentityResult> CreateCountryClub(CountryClubPlace countryClub)
        {
            try
            {
                Address address = countryClub.Address;
                _context.Add(address);
                await _context.SaveChangesAsync();
                int addressId = address.Id;
                countryClub.AddressId = addressId;
                _context.Add(countryClub);
                await _context.SaveChangesAsync();
                return IdentityResult.Success;
            }
            catch (Exception e)
            {
                return IdentityResult.Failed(new IdentityError()
                {
                    Code = e.GetType().ToString(),
                    Description = e.Message
                });
            }
        }

        public IEnumerable<CountryClubPlace> GetAllCountryClubs()
        {
            var result = _context.CountryClubs.Include(e => e.Address).ToList();
            return result;
        }
    }
}
