﻿using CountryClub.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountryClub.Business.Services
{
    public interface IEmployerService
    {
        IEnumerable<Employer> GetAllEmployers();
        Task<IdentityResult> CreateEmployer(Employer employer);
        Task<IdentityResult> UpdateEmployer(string id, Employer employer);
    }
}
